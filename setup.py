from setuptools import setup, find_packages

setup(
    # name of package
    name="snapshot",
    # packages (directories) to be included
    packages=find_packages(),
    # script entry point
    entry_points={
        "console_scripts": [
            "snapshot = snapshot.snapshot:main",
        ],
    },
    # package dependencies
    install_requires=[
        "attrs==22.1.0",
        "distlib==0.3.6",
        "filelock==3.8.0",
        "flake8==3.9.2",
        "freezegun==1.1.0",
        "iniconfig==1.1.1",
        "mccabe==0.6.1",
        "mock==4.0.3",
        "packaging==21.3",
        "parameterized==0.8.1",
        "platformdirs==2.5.2",
        "pluggy==1.0.0",
        "psutil==5.9.2",
        "py==1.11.0",
        "pycodestyle==2.7.0",
        "pyflakes==2.3.1",
        "pyparsing==3.0.9",
        "pytest==7.1.2",
        "python-dateutil==2.8.2",
        "six==1.16.0",
        "toml==0.10.2",
        "tomli==2.0.1",
        "tox==3.23.1",
        "virtualenv==20.16.5"
    ],
    version="1.0",
    author="Aleksey Pivchenkov",
    author_email="aleksey_pivchenkov@epam.com",
    description="Snapshot application",
    license="MIT")
