"""
Make snapshot

{"Tasks": {"total": 440, "running": 1, "sleeping": 354, "stopped": 1, "zombie": 0},
"%CPU": {"user": 14.4, "system": 2.2, "idle": 82.7},
"KiB Mem": {"total": 16280636, "free": 335140, "used": 11621308},
"KiB Swap": {"total": 16280636, "free": 335140, "used": 11621308},
"Timestamp": 1624400255}
"""
import argparse
import psutil
import time
import os
import json


class SystemData:
    result = {
        "Tasks": {"total": "", "running": "", "sleeping": "", "stopped": "", "zombie": ""},
        "%CPU": {"user": "", "system": "", "idle": ""},
        "KiB Mem": {"total": "", "free": "", "used": ""},
        "KiB Swap": {"total": "", "free": "", "used": ""},
        "Timestamp": ""
    }

    def __init__(self, file_name) -> None:
        self.file_name = file_name

    def collect_data(self):
        tasks_status = {"running": 0, "sleeping": 0, "stopped": 0, "zombie": 0}
        tasks = psutil.pids()
        for task in tasks:
            if psutil.Process(task).status() == "running":
                tasks_status["running"] += 1
            elif psutil.Process(task).status() == "sleeping":
                tasks_status["sleeping"] += 1
            elif psutil.Process(task).status() == "stopped":
                tasks_status["stopped"] += 1
            elif psutil.Process(task).status() == "zombie":
                tasks_status["zombie"] += 1

        self.result["Tasks"]["total"] = len(tasks)
        self.result["Tasks"]["running"] = tasks_status["running"]
        self.result["Tasks"]["sleeping"] = tasks_status["sleeping"]
        self.result["Tasks"]["stopped"] = tasks_status["stopped"]
        self.result["Tasks"]["zombie"] = tasks_status["zombie"]

        cpu = psutil.cpu_times_percent()
        self.result["%CPU"]["user"] = cpu.user
        self.result["%CPU"]["system"] = cpu.system
        self.result["%CPU"]["idle"] = cpu.idle

        ram = psutil.virtual_memory()
        self.result["KiB Mem"]["total"] = ram.total // 1024
        self.result["KiB Mem"]["free"] = ram.free // 1024
        self.result["KiB Mem"]["used"] = ram.used // 1024

        swap = psutil.swap_memory()
        self.result["KiB Swap"]["total"] = swap.total // 1024
        self.result["KiB Swap"]["free"] = swap.free // 1024
        self.result["KiB Swap"]["used"] = swap.used // 1024

        self.result["Timestamp"] = int(time.time())

    def write_data(self):
        with open(self.file_name, "a") as file:
            file.write(json.dumps(self.result) + "\n")


def main():
    """Snapshot tool."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help="Interval between snapshots in seconds", type=int, default=5)
    parser.add_argument("-f", help="Output file name", default="snapshot.json")
    parser.add_argument("-n", help="Quantity of snapshot to output", default=20)
    args = parser.parse_args()

    os.system("clear")
    open(args.f, 'w').close()

    for i in range(int(args.n)):
        data = SystemData(args.f)
        data.collect_data()
        data.write_data()
        print(data.result, end="\r")
        time.sleep(args.i)


if __name__ == "__main__":
    main()
